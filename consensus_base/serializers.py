from django.core.serializers.python import Serializer

from .helpers import is_admin
from .helpers import get_num_of_questions_total
from .helpers import get_invitations
from .helpers import get_participants
from .helpers import get_participants_pending
from .helpers import get_questions_pending


class PollSerializer(Serializer):
    """
    Serializes a model.Poll in a regular dict
    """
    def __init__(self, username):
        self.username = username

    def end_object(self, obj):
        """
        Called when serializing of an object ends.
        """
        fields = ["title", "slug", "uuid", "description", "published",
                  "creator", "locked", "public", "startdate", "enddate", "customreminder"]
        data = (self.get_dump_object(obj))
        dict_ = {}
        for f in fields:
            dict_[f] = data["fields"][f]

        dict_["isadmin"] = is_admin(obj, self.username)
        dict_["numofparticipants"] = get_participants(obj.id)[0]
        dict_["numofacceptedinvitations"] = get_invitations(obj.id, "accepted")[0]
        dict_["numofinvitations"] = get_invitations(obj.id)[0]
        dict_["numofpendingvotes"] = get_participants_pending(obj.id)[0]
        dict_["numofquestions"] = get_num_of_questions_total(obj.id)

        self.objects.append(dict_)
        self._current = None


class InvitationSerializer(Serializer):
    """
    Serializes a model.Invitations in a regular dict
    """

    def end_object(self, obj):
        """
        Called when serializing of an object ends.
        """
        fields = ["poll", "username", "email", "used",
                  "sent", "status", "token", "created_by_username", "sentdate", "visiteddate"]
        data = (self.get_dump_object(obj))
        dict_ = {}
        for f in fields:
            dict_[f] = data["fields"][f]
        dict_["polltitle"] = obj.poll.title
        dict_["pollslug"] = obj.poll.slug
        dict_["polldescription"] = obj.poll.description
        self.objects.append(dict_)
        self._current = None


class ParticipantsSerializer(Serializer):
    """
    Serializes a model.Participants from a given poll into a regular dict
    """

    def __init__(self, poll):
        self.poll = poll

    def end_object(self, obj):
        fields = ["username", "admin", "poll"]
        data = (self.get_dump_object(obj))
        dict_ = {}
        for f in fields:
            dict_[f] = data["fields"][f]
        dict_["ispolladmin"] = is_admin(self.poll, dict_["username"])
        dict_["numpendingquestions"] = get_questions_pending(obj.poll.id, dict_ ["username"])[0]
        self.objects.append(dict_)
        self._current = None
