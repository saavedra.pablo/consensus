Setup Consensus using Ansible
=============================

Ansible directory layout
^^^^^^^^^^^^^^^^^^^^^^^^

::

    .
    ├── ansible.cfg
    ├── consensus.yml
    ├── external
    │   └── consensus -> /local/git/django_consensus (Gitlab Git repo.)
    ├── hosts
    └── host_vars
        └── consensus.yml

Content of the Ansible files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is the content and the purpose of each file:

`external/consensus -> /local/git/django_consensus` directopry
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* This a reference to the django_consensus Git repository. This repository
  contains an Ansible directory with the defined roles.
* Content::

      external/consensus/ansible/roles/
      ├── djangoconsensusapache2 (Apache2 required packages and configuration)
      ├── djangoconsensusapp (Django app required packages and configuration)
      └── djangoconsensuspsql (Postgresql required packages and configuration)

`ansible.cfg` file
~~~~~~~~~~~~~~~~~~

* The configuration file used by Ansible.
* Content::

       [defaults]
       hostfile = hosts
       library = /usr/share/ansible
       module_name = command
       remote_tmp = $HOME/.ansible/tmp
       pattern = *
       forks=5
       timeout=50
       poll_interval=15
       sudo_user=root
       transport=ssh
       remote_port=22
       remote_user=root
       sudo_exe=sudo
       ansible_managed = Ansible managed: {file} modified on %Y-%m-%d %H:%M:%S by {uid} on {host}
       action_plugins     = /usr/share/ansible_plugins/action_plugins
       callback_plugins   = /usr/share/ansible_plugins/callback_plugins
       connection_plugins = /usr/share/ansible_plugins/connection_plugins
       lookup_plugins     = /usr/share/ansible_plugins/lookup_plugins
       vars_plugins       = /usr/share/ansible_plugins/vars_plugins
       filter_plugins     = /usr/share/ansible_plugins/filter_plugins

       [ssh_connection]
       ssh_args=-C -o PasswordAuthentication=no -o ControlPath=/tmp/ansible-ssh-%h-%p-%r -o ControlMaster=auto -o ControlPersist=600s -o UserKnownHostsFile=./known_hosts -o StrictHostKeyChecking=no

`hosts` file
~~~~~~~~~~~~

* The inventory of the hosts to be managed by the Ansible playbooks.
* Content::

       [consensus_hosts]
       consensus ansible_ssh_host=192.168.1.101

`consensus.yml` file
~~~~~~~~~~~~~~~~~~~~

* The Ansible playbook. This file links hosts and/or groups from the
  inventory to the roles defined in the Ansible recipes
* Content::

       - hosts:
           - consensus_hosts
         roles:
           - { role: external/consensus/ansible/roles/djangoconsensusapache2 }
           - { role: external/consensus/ansible/roles/djangoconsensuspsql }
           - { role: external/consensus/ansible/roles/djangoconsensusapp }

`host_vars/consensus.yml` file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Custom overwritings for the host `consensus`.
* Content::

       ---
       ### djangoconsensusapache2
       djangoconsensusapache2_oidc: False
       djangoconsensusapache2_server_name: consensus.gvatas.in

       djangoconsensusapache2_virtual_port: 8080
       djangoconsensusapache2_virtual_port_ssl: 8443

       djangoconsensusapache2_apache2_ssl: |
         SSLCertificateFile /etc/letsencrypt/live/gvatas.in/fullchain.pem
           SSLCertificateKeyFile /etc/letsencrypt/live/gvatas.in/privkey.pem
           Header always set Strict-Transport-Security "max-age=31536000; includeSubdomains;"
           RequestHeader set X_FORWARDED_PROTO 'https'
           RequestHeader set X-Forwarded-Ssl on

       djangoconsensusapache2_django_consensus: /opt/django_consensus
       djangoconsensusapache2_django_consensus_media: /opt/django_consensus/media
       djangoconsensusapache2_django_consensus_static: /opt/django_consensus/static

       djangoconsensusapache2_django_consensus_server: unix:/run/django_consensus.socket

       djangoconsensusapache2_apache2_server_custom: |
         # Require valid-user all granted
             Order Deny,Allow
             Allow from all
             # Deny from all
             # Allow from 192.168.0.0/16
             # Allow from 127.0.0.0/8

       djangoconsensusapache2_apache2_admin_location_custom: |
         # Require valid-user all granted
             Order Deny,Allow
             Allow from all
             # Deny from all
             # Allow from 192.168.0.0/16
             Allow from 127.0.0.0/8
             Deny from all

       ### djangoconsensuspsql
       djangoconsensuspsql_psql_database: djangoconsensus
       djangoconsensuspsql_psql_user: djangoconsensus
       djangoconsensuspsql_psql_password: djangoconsensus_replace_me

       ### djangoconsensusapp
       djangoconsensusapp_server_name: consensus.gvatas.in
       djangoconsensusapp_django_consensus_git_version: main

       djangoconsensusapp_django_consensus_tests_run: 0
       djangoconsensusapp_django_consensus_tests:
         - py3-django
         - py3-pep8

       djangoconsensusapp_django_consensus_log_level: debug

       djangoconsensusapp_django_consensus_local_settings_custom: |
         from .settings import *
         DEBUG = True
         DATABASES = {
           'default': {
             'ENGINE': 'django.db.backends.postgresql_psycopg2',
             'NAME': '{{ djangoconsensuspsql_psql_database }}',
             'USER': '{{ djangoconsensuspsql_psql_user }}',
             'PASSWORD': '{{ djangoconsensuspsql_psql_password }}',
             'HOST': 'localhost',
             'PORT': '',
           }
         }
         ALLOWED_HOSTS = [
           "consensus.gvatas.in" ,
           "consensus" ,
           "localhost"
         ]
         ###############################################################################
         # SESSION
         ###############################################################################
         SESSION_COOKIE_HTTPONLY = False  # XXX Very important keep this false to allow
         # session refresh and logouts from the SSO.
         SESSION_COOKIE_AGE = 60 * 60 * 48
         ###############################################################################
         # AUTHENTICATION
         ###############################################################################
         AUTHENTICATION_BACKENDS = (
             # 'django.contrib.auth.backends.RemoteUserBackend',
             # 'django_auth_ldap.backend.LDAPBackend',
             'django.contrib.auth.backends.ModelBackend',
             'social_core.backends.open_id.OpenIdAuth',
             'social_core.backends.google.GoogleOpenId',
             'social_core.backends.google.GoogleOAuth2',
             'social_core.backends.github.GithubOAuth2',
             'social_core.backends.gitlab.GitLabOAuth2',
             'social_core.backends.twitter.TwitterOAuth',
             'social_core.backends.facebook.FacebookOAuth2',
         )
         ###############################################################################
         # consensus settings
         ###############################################################################
         CONSENSUS_SUPPORT_URL = "https://domain_url"
         CONSENSUS_PROFILE_URL = "https://domain_url"
         CONSENSUS_LOGOUT_URL = "https://consensus.gvatas.in/logout/?next=https://consensus.gvatas.in"
         CONSENSUS_DEFAULT_ALLOWED_GROUPS = ['users']

         CONSENSUS_MAIL_SENDER = "no-reply@gvatas.in"
         CONSENSUS_MAIL_POLL_URL_PATTERN = "https://consensus.gvatas.in/poll/%s"

         EMAIL_HOST="127.0.0.1"
         EMAIL_PORT=25

         LOGIN_URL = 'login'
         LOGOUT_URL = 'logout'
         LOGIN_REDIRECT_URL = '/'

         USERNAME_IS_FULL_EMAIL = True

         SOCIAL_AUTH_GITLAB_KEY = ''
         SOCIAL_AUTH_GITLAB_SECRET = ''

         SOCIAL_AUTH_GITHUB_KEY = ''
         SOCIAL_AUTH_GITHUB_SECRET = ''

         SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = ''
         SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = ''

         ###############################################################################
         # Logging
         # ################################################################################
         LOGGING['root']['level'] = 'INFO'

Rolling out Consensus using the Ansible playbook
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    # Run in dry-mode seeing the differences only for the host 'consensus'
    ansible-playbook -i hosts -C --diff --limit consensus consensus.yml

::

    # Apply roles for all the corresponding hosts associated to
    # the 'consensus.yml' playbook.
    ansible-playbook -i hosts consensus.yml


Other Ansible documentation
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* `Ansible Quickstart <https://docs.ansible.com/ansible/latest/user_guide/quickstart.html>`_
* `Get Started in Ansible <https://www.ansible.com/resources/get-started>`_

