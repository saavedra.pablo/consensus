#!/bin/sh
[ -f /run/secrets/local_settings.py ] && cp /run/secrets/local_settings.py django_consensus/
[ -f /run/secrets/supervisor-celery.conf ] && cp /run/secrets/supervisor-celery.conf /etc/supervisor/conf.d/
[ -f /run/secrets/supervisor-redis.conf ] && cp /run/secrets/supervisor-redis.conf /etc/supervisor/conf.d/
[ -f /run/secrets/redis.conf ] && cp /run/secrets/redis.conf /etc/redis/
[ -e media ] || mkdir media
python3 manage.py wait_for_db
python3 manage.py makemigrations --no-input
python3 manage.py migrate --no-input
python3 manage.py collectstatic --no-input
