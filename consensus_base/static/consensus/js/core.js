var poll_status;

function end_tour(){
    $("#button-tour").removeClass('invisible');
}

function launch_tour(tour){
    $("#sidebar").addClass('active');
    setTimeout(function(){
        tour.trigger('depart.tourbus');
    }, 500);
    $("#button-tour").addClass('invisible');
}

function first_list_element_by_key(list, key, value){
    for (var e in list) {
        if (list.hasOwnProperty(e)) {
            if (list[e].hasOwnProperty(key)) {
                if (list[e][key] == value) {
                    return e
                }
            }
        }
    }
    return false
}

function show_error_message(message) {
    $('#navbar-message-action').html('Error: ' + message);
}

function show_save_message() {
    now = new Date();
    $('#navbar-message-save').html('<b><i>Last saved on ' + now + '</i></b>');

    setTimeout(function () {
        $('#navbar-message-save').html('Last edition on ' + now);
    }, 500)
}

function clean_action_message() {
    $('#navbar-message-action').html('');
}
