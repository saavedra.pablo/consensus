Consensus's documentation!
==========================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   installation
   development
   license
