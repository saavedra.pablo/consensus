# django_consensus

## Quick start

Setting up the enviroment::

``` sh
sudo apt install python-is-python3 python3-virtualenv
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
export PATH=$PATH:~/.local/bin

# clone django_consensus repository
git clone ssh://git@gitlab.com/saavedra.pablo/consensus.git
cd django_consensus

sudo apt-get install libpq-dev build-essential \
    libsasl2-dev libldap2-dev libssl-dev \
    python3-dev supervisor redis-server redis-tools tox \
    zlib1g-dev libjpeg-dev

sudo apt-get install sphinx-doc sphinx-common  # To build the documentation.

virtualenv -p python3  env
source env/bin/activate
pip install -r requirements.txt

# Setup
python manage.py migrate
python manage.py creatersakey

# ...
./manage.py runsslserver 0.0.0.0:8443

# ...
deactivate
```

Any kind of SSL errors like `ssl.SSLEOFError: EOF occurred in violation of
protocol (_ssl.c:1949)`?  Try with this:

``` sh
pip install --force-reinstall requests[security]
```
Ref: https://github.com/jakubroztocil/httpie/issues/315

## Running Consensus with Docker compose

* Visit the [docker-compose section in Consensus's Read the Docs](https://get-consensus.gitlab.io/consensus/docker-compose.html)
  documentation.

## Development

* Visit the
  [development section in Consensus's Read the Docs](https://get-consensus.gitlab.io/consensus/development.html)
  to learn about the codestyle guides, files structure and similar topics.
* **For testing**, visit the
  [testing section in Consensus's Read the Docs](https://get-consensus.gitlab.io/consensus/testing.html)
  to learn about how to run the test suites.

## Documentation

* Visit [Consensus's Read the Docs documentation](https://get-consensus.gitlab.io/consensus/)
  for more resources about the installation, configuration, development and
  others topics.

* Build documentation in local:

  ``` sh
  cd docs
  make html
  ```
