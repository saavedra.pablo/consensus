# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import logging
import pytz
import uuid
import time
from jsoneditor.fields.django_jsonfield import JSONField

from django.db import models
from django.db.models import Q
from django.conf import settings as s
from django.utils.crypto import get_random_string
from django.utils import timezone

from .utils import unique_slugify

logger = logging.getLogger(__name__)

INVITATION_STATUS_ACCEPTED = 'accepted'
INVITATION_STATUS_DECLINED = 'declined'
INVITATION_STATUS_PENDING = 'pending'
INVITATION_STATUS_REVOKED = 'revoked'
INVITATION_STATUS_SENT = 'sent'


class PollManager(models.Manager):

    def _Q_is_admin(self, username):
        return Q(pollparticipant__admin=True,
                 pollparticipant__username=username) | Q(creator = username)

    def _Q_is_participant(self, username):
        return Q(public = True) | Q(pollparticipant__username=username)

    def where_is_admin(self, username):
        return super().filter(self._Q_is_admin(username))

    def where_is_participant(self, username):
        return super().filter(self._Q_is_participant(username))

    def where_is_visible(self, username):
        return super().filter(Q(Q(published=True) &
                                self._Q_is_participant(username)) |
                              self._Q_is_admin(username))


class Poll(models.Model):
    objects = PollManager()
    creatorid = models.IntegerField(blank=True, null=True)
    creator = models.CharField(blank=True, null=True, max_length=250)
    title = models.CharField(max_length=1000)
    slug = models.SlugField(blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    description = models.TextField(max_length=10000)
    published = models.BooleanField(default=False)
    locked = models.BooleanField(default=False)
    public = models.BooleanField(default=False)
    startdate = models.DateTimeField(
        null=True, blank=True, db_index=True
    )
    enddate = models.DateTimeField(
        null=True, blank=True, db_index=True
    )
    customreminder = models.CharField(max_length=10000,blank=True)

    def is_active(self):
        if not self.published:
            return False
        if self.locked:
            return False
        if self.startdate and self.startdate > timezone.now():
            return False
        if self.enddate and self.enddate < timezone.now():
            return False
        return True

    def __str__(self):
        return self.title

    def save(self, **kwargs):
        slug_str = "%s" % (self.title)
        unique_slugify(self, slug_str)
        if not self.uuid:
            self.uuid = uuid4()
        super(Poll, self).save(**kwargs)


class PollChange(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    poll = models.ForeignKey(Poll)
    change = JSONField(
        blank=True,
        default={},
    )
    ctime = models.DateTimeField(
            null=True, blank=True, db_index=True
    )

    def __str__(self):
        return self.uuid.hex

    def save(self, **kwargs):
        if not self.ctime:
            self.ctime = datetime.datetime.fromtimestamp(time.time(), pytz.UTC)
        if not self.uuid:
            self.uuid = uuid4()
        super(PollChange, self).save(**kwargs)


class PollInvitationManager(models.Manager):
    def actives(self):
        return super().filter(sent=True, status=INVITATION_STATUS_SENT)


class PollInvitation(models.Model):
    objects = PollInvitationManager()
    poll = models.ForeignKey(Poll)
    username = models.CharField(null=True, blank=True, max_length=250)
    email = models.CharField(max_length=250)
    used = models.BooleanField(default=False)
    sent = models.BooleanField(default=False)
    status = models.CharField(max_length=250)
    token = models.CharField(max_length=64, unique=True)
    created_by_username = models.CharField(max_length=250, blank=True)

    sentdate = models.DateTimeField(
        null=True, blank=True, db_index=True
    )
    visiteddate = models.DateTimeField(
        null=True, blank=True, db_index=True
    )

    def accept(self, username, save=True):
        self.username = username
        self.used = True
        self.status = INVITATION_STATUS_ACCEPTED
        self.visiteddate = timezone.now()
        if save:
            self.save()

    def decline(self, username, save=True):
        self.username = username
        self.used = True
        self.status = INVITATION_STATUS_DECLINED
        self.visiteddate = timezone.now()
        if save:
            self.save()

    def resend(self, save=True):
        self.used = False
        self.sent = False
        self.status = INVITATION_STATUS_PENDING
        if save:
            self.save()

    def revoke(self, save=True):
        self.used = True
        self.status = INVITATION_STATUS_REVOKED
        if save:
            self.save()

    def save(self, **kwargs):
        if not self.token:
            self.token = get_random_string(length=64)
        super(PollInvitation, self).save(**kwargs)


class PollParticipant(models.Model):
    username = models.CharField(max_length=250)
    poll = models.ForeignKey(Poll)
    admin = models.BooleanField(default=False)


class PollGroupParticipant(models.Model):
    group = models.CharField(max_length=250)
    poll = models.ForeignKey(Poll)


class PollSection(models.Model):
    title = models.CharField(max_length=1000)
    slug = models.SlugField(blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    help = models.TextField(max_length=10000)
    poll = models.ForeignKey(Poll)
    hide = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def save(self, **kwargs):
        slug_str = "%s" % (self.title)
        unique_slugify(self, slug_str)
        if not self.uuid:
            self.uuid = uuid4()
        super(PollSection, self).save(**kwargs)


class PollQuestion(models.Model):
    question = models.CharField(max_length=1000)
    slug = models.SlugField(blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    help = models.TextField(max_length=10000)
    section = models.ForeignKey(PollSection)
    typeofquestion = models.CharField(
        default=getattr(s, 'CONSENSUS_POLL_QUESTION_TYPE_DEFAULT', ""),
        choices=getattr(s, 'CONSENSUS_POLL_QUESTION_TYPE_CHOICES', ()),
        max_length=250,
    )
    hide = models.BooleanField(default=False)
    mandatory = models.BooleanField(default=False)
    properties = JSONField(
        blank=True,
        default=[
            {"Property (ex: Max. Weight)": "Value (ex: 100)"},
        ]
    )
    order = models.IntegerField(default=0)
    choices = JSONField(
        blank=True,
        default={
            1: "Not at all",
            2: "Not very interesting",
            3: "Interesting",
            4: "Very interesting",
            5: "It is what we need",
            6: "Abstention",
        }
    )

    def __str__(self):
        return self.question

    def save(self, **kwargs):
        slug_str = "%s" % (self.question)
        unique_slugify(self, slug_str)
        if not self.uuid:
            self.uuid = uuid4()
        super(PollQuestion, self).save(**kwargs)


class PollQuestionAnswer(models.Model):
    question = models.ForeignKey(PollQuestion)
    mtime = models.DateTimeField(
            auto_now_add=True, null=True, blank=True, db_index=True
    )
    userid = models.IntegerField()
    username = models.CharField(max_length=250)

    answer = JSONField(
        blank=True,
        default={
            "1": "Choice 1",
            "2": "Choice 2",
        }
    )

    def __str__(self):
        return "[Poll Question Answer] [%s] [%s]" % (self.question,
                                                     self.username)

    def save(self, **kwargs):
        d = datetime.datetime.fromtimestamp(time.time(), pytz.UTC)
        if not self.mtime:
            self.mtime = d

        super(PollQuestionAnswer, self).save(**kwargs)


class PollQuestionComment(models.Model):
    question = models.ForeignKey(PollQuestion)
    mtime = models.DateTimeField(
            auto_now_add=True, null=True, blank=True, db_index=True
    )
    userid = models.IntegerField()
    username = models.CharField(max_length=250)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    comment = models.TextField(max_length=10000)

    def __str__(self):
        return "[Poll Question Comment] [%s] [%s]" % (self.question,
                                                      self.username)

    def save(self, **kwargs):
        d = datetime.datetime.fromtimestamp(time.time(), pytz.UTC)
        if not self.mtime:
            self.mtime = d
        if not self.uuid:
            self.uuid = uuid4()

        super(PollQuestionComment, self).save(**kwargs)
