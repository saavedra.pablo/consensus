from django.conf import settings as s
from django.core.management.base import BaseCommand, CommandError

from consensus_base.helpers import get_participants_pending
from consensus_base.mail import send_reminder
from consensus_base.models import Poll

class Command(BaseCommand):
    help = 'Send reminders for the peding votes'

    def add_arguments(self, parser):
        parser.add_argument(
            '--poll',
            dest='poll',
            help='Run for a specific poll',
        )

    def handle(self, *args, **options):
        polls = Poll.objects.filter(locked=False, published=True)
        if options['poll']:
            polls = polls.filter(uuid=options['poll'])

        for poll in polls:
            for participant in get_participants_pending(poll.id)[1]:
                try:
                    send_reminder(poll, participant)
                except Poll.DoesNotExist:
                    self.stdout.write(self.style.ERROR('Problem sending reminder to %s for %s' % (participant, poll)))  # noqa: E501
            self.stdout.write(self.style.SUCCESS('Reminders sent for "%s"' % poll))  # noqa: E501
