import logging

from django.conf import settings as s
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import validate_email as django_validate_email
from django.utils import timezone
from smtplib import SMTPException

from .models import INVITATION_STATUS_SENT
from .models import PollInvitation

logger = logging.getLogger(__name__)


def validate_email(email):
    try:
        django_validate_email(email)
        return True
    except ValidationError:
        return False


def send_invitation(invitation, resend=True):
    poll = invitation.poll
    to = invitation.email

    if not invitation.poll.is_active():
        logger.info("Skip send invitation to '%s' for '%s': Poll is not active",  # noqa: E501
                    to, poll, resend)
        return

    if not resend and invitation.sent:
        logger.info("Skip send invitation to '%s' for '%s': Already sent (resend=%s)",  # noqa: E501
                    to, poll, resend)
        return

    try:
        sender = User.objects.get(username=invitation.created_by_username)
    except User.DoesNotExist:
        sender = None
    by = "" if not sender else " by " + ("%s %s <%s>" % (sender.first_name,
                                                         sender.last_name,
                                                         sender.email)).strip()
    try:
        poll_url = getattr(s, "CONSENSUS_MAIL_POLL_URL_PATTERN", "%s") % poll.slug  # noqa: E501
        poll_url += "?invitation=%s" % invitation.token
        sender = getattr(s, "CONSENSUS_MAIL_SENDER", "noreply@consensus.local")
        send_mail("[poll] Request for your participation in '%s'" % poll,
                  """Hi,

you have been invited%s to participate in the '%s' poll. Please,
visit the following link to accept and start your participation:

    %s

Thanks.
""" % (by, poll, poll_url), sender, [to], fail_silently=False)

        invitation.sent = True
        invitation.sentdate = timezone.now()
        invitation.status = INVITATION_STATUS_SENT
        invitation.save()
        logger.info("Inviation for '%s' sent to '%s'", poll, to)
    except SMTPException as e:
        logger.warning("Problem sending invitation for '%s' to '%s': %s",
                       poll, to, e)
        invitation.sent = True
        invitation.sentdate = timezone.now()
        invitation.status = "failed: %s" % e
        invitation.save()


def send_invitations():
    invitations = PollInvitation.objects.filter(sent=False,
                                                poll__published=True,
                                                poll__locked=False)
    for i in invitations:
        send_invitation(i)
        pass


def send_reminder(poll, username):
    if not poll.is_active():
        logger.info("Skip send reminder to '%s' for '%s': Poll is not active",
                    to, poll, resend)
        return

    user = User.objects.get(username = username)
    poll_url = getattr(s, "CONSENSUS_MAIL_POLL_URL_PATTERN", "%s") % poll.slug
    sender = getattr(s, "CONSENSUS_MAIL_SENDER", "noreply@consensus.local")
    reminder = poll.customreminder
    if not poll.customreminder:
        send_mail(
            "[poll] Pending votes in '%s'" % poll,
            """Hello %s,

    we have noticed that you have pendings votes in '%s'. Please complete
    the poll with your choices visiting this URL:

        %s

    Thanks!!!
    """ 
        % (user.get_short_name(),poll, poll_url),
            sender,
            [user.email],
            fail_silently=False,
        )
    else:
            send_mail(
            "[poll] Pending votes in '%s'" % poll,
            " %s \n %s \n %s \n " % (reminder,poll,poll_url),
            sender,
            [user.email],
            fail_silently=False,
        )
