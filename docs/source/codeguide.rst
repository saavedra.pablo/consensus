Code Guide
==========

Project directory layout
^^^^^^^^^^^^^^^^^^^^^^^^

::

  django_consensus_site
    __init__.py
    settings.py
    urls.py
    wsgi.py
  app1
    migrations
    __init__.py
    admin.py
    apps.py
    models.py
    tests.py
    views.py
  static
    css
      main.cs
    js
      main.js
  manage.py

The django_consensus_site has a base template for the project (`base.html`) with the
header and footer and a `{%block content%}` for your main content.

Have your other templates inherit from the `base.html`
`{% extends "base.html" %}` and override the content section.

As long as the apps are in `INSTALLED_APPS` and the template loader for apps
dirs  is enabled, we can include any template from another app::

    {% include "header.html" %}

The templates are located directly in the templates dir of the apps. Generally,
in order to avoid name clashes it is better to use::

    app1/
      templates/
    app1/
      page1.html
      page2.html
    app2/
      templates/
    app2/
      page1.html
      page2.html

And `{% include "app1/page1.html" %}` or  `{% include "app2/page1.html" %}`.

Branching
^^^^^^^^^

* `<feature>`: Branch dedicated to a especific branch. Must be synced with
  `develop`
* `<username>`: Integration area for each developer (many features can be
  included in each developer branch). Must be synced with `develop`
* `develop`: Project integration stage. Usually is the consolidation from each
  developer branch and or feature.
  * The code must accomplish with the unit tests before to be merged here
* `main`: Code ready to *production* stage. Usually is a merge from `develop`.
  * Only code which passes all the suite test can reach this stage

How to refresh the Django project requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    pip freeze > requirements.txt


