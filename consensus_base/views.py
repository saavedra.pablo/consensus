# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

# import datetime
import json
# import pytz
# import time


from django.conf import settings as s
# from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.http import JsonResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.template import loader
# from django.utils.translation import ugettext as _
# from django.core.files import File
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import CreateView

from .forms import PollForm
use_ldap = getattr(s, "CONSENSUS_USE_LDAP", False)
if use_ldap:
    from .forms import PollSettingsForm
else:
    from .forms import PollSettingsSimpleForm as PollSettingsForm
from .forms import UploadFileForm
from .models import Poll
from .models import PollChange
from .models import PollInvitation
from .models import PollQuestion
from .models import PollQuestionAnswer
from .models import PollQuestionComment
from .models import PollSection
from .tasks import simulate_send_emails
from .mail import validate_email
from .utils import from_date_to_timestamp
from .utils import is_valid_uuid
from .utils import paginator_page
from .utils import sort_by
from .helpers import create_invitations
from .helpers import create_poll_change
from .helpers import get_comment_status
from .helpers import get_num_of_participants
from .helpers import get_num_of_questions_total
from .helpers import get_questions_pending
from .helpers import get_question_status
from .helpers import get_questions_status
from .helpers import get_overall_status
from .helpers import get_user_answer
from .helpers import is_admin
from .helpers import is_participant
from .helpers import is_poll_visible_by_user
from .helpers import set_participant
from .helpers import set_participants
from .serializers import PollSerializer
from .serializers import InvitationSerializer
from .serializers import ParticipantsSerializer

logger = logging.getLogger(__name__)


@login_required
def index(request):
    template = loader.get_template('consensus_base/index.html')
    context = {}
    return HttpResponse(template.render(context, request))


@sort_by
@login_required
def invitation(request, token):
    try:
        objs = PollInvitation.objects
        i = objs.actives().get(token=token)
        c = {'invitation': i}
        return render(request, 'consensus_base/invitation.html', c)
    except Exception as e:
        logger.debug("Problem processing invitation view for the %s token: %s",
                     token, e)
        return redirect('consensus_base:index')


@login_required
def invitation_action(request, token, action):
    next_page = request.GET.get('next_page', 'consensus_base:index')
    if action == "accept":
        try:
            invitation = PollInvitation.objects.get(token=token,
                                                    used=False)

            set_participant(invitation.poll, request.user.username)
            invitation.accept(request.user.username)
            logger.debug("Invitation %s : accepted by the user", token)
            return redirect(next_page)
        except Exception as e:
            logger.debug("Problem accepting invitation %s: %s", token, e)
            return redirect(next_page)
    if action == "decline":
        try:
            invitation = PollInvitation.objects.get(email=request.user.email,
                                                    token=token,
                                                    used=False)
            invitation.decline(request.user.username)
            logger.debug("Invitation %s : declined by the user", token)
            return redirect(next_page)
        except Exception as e:
            logger.debug("Problem declining invitation %s: %s", token, e)
            return redirect(next_page)
    if action == "resend":
        try:
            invitation = PollInvitation.objects.get(token=token)
            if not is_admin(invitation.poll, request.user.username):
                raise Exception()
            invitation.resend()
            logger.debug("Invitation %s : resent", token)
            return redirect(next_page)
        except Exception as e:
            logger.debug("Problem revoking invitation %s: %s", token, e)
            return redirect(next_page)
    if action == "revoke":
        try:
            invitation = PollInvitation.objects.get(token=token)
            if not is_admin(invitation.poll, request.user.username):
                raise Exception()
            invitation.revoke()
            logger.debug("Invitation %s : revoked", token)
            return redirect(next_page)
        except Exception as e:
            logger.debug("Problem revoking invitation %s: %s", token, e)
            return redirect(next_page)
    return redirect(next_page)


@sort_by
@login_required
def invitations(request):
    pag_items = getattr(s, "CONSENSUS_POLLS_PAGINATOR_ITEMS", 25)
    page = request.GET.get('page')

    objs = PollInvitation.objects
    invitations = objs.actives().filter(email=request.user.email)
    if request.sort_by:
        invitations = invitations.order_by(request.sort_by)
    else:
        invitations = invitations.order_by('-sentdate')

    paginator = Paginator(
        invitations,
        pag_items
    )

    invitations = paginator_page(paginator, page)

    c = {
        'paginated_items': invitations
    }
    return render(request, 'consensus_base/invitations.html', c)


@login_required
def poll(request, slug):
    invitation = request.GET.get('invitation', None)
    if invitation:
        return redirect('consensus_base:invitation', invitation)

    template = loader.get_template('consensus_base/poll.html')
    # TODO, make it a decorator
    poll = get_poll_by_uuid_or_slug(slug=slug)

    if not is_poll_visible_by_user(poll, request.user.username):
        return HttpResponseForbidden()
    context = {
        'poll': poll
    }
    return HttpResponse(template.render(context, request))


@login_required
def poll_action(request, uuid, action):
    poll = Poll.objects.get(uuid=uuid)
    if not is_admin(poll, request.user.username):
        return HttpResponseForbidden()
    next_page = request.GET.get('next_page', 'consensus_base:index')
    username = request.GET.get('username', '')
    if action == "lock":
        poll.locked = True
        create_poll_change(poll, "poll_locked")
    if action == "unlock":
        poll.locked = False
        create_poll_change(poll, "poll_unlocked")
    if action == "publish":
        poll.published = True
        create_poll_change(poll, "poll_published")
    if action == "unpublish":
        poll.published = False
        create_poll_change(poll, "poll_unpublished")
    if action == "grant_admin":
        poll.pollparticipant_set.filter(username=username).update(admin=True)
    if action == "revoke_admin":
        poll.pollparticipant_set.filter(username=username).update(admin=False)
    if action == "remove":
        poll.pollparticipant_set.filter(username=username).delete()
    poll.save()
    return redirect(next_page)


@login_required
def poll_delete(request, slug):
    template = loader.get_template('consensus_base/poll_delete.html')
    # TODO, make it a decorator
    poll = get_poll_by_uuid_or_slug(slug=slug)

    if not is_admin(poll, request.user.username):
        return HttpResponseForbidden()
    if request.method == 'POST':
        create_poll_change(poll, "poll_deleted")
        poll.delete()
        return redirect('consensus_base:index')
    context = {}
    context['poll'] = poll
    return HttpResponse(template.render(context, request))


@login_required
def poll_formatted(request, slug):
    template = loader.get_template('consensus_base/poll_formatted.txt')
    # TODO, make it a decorator
    poll = get_poll_by_uuid_or_slug(slug=slug)

    if not is_poll_visible_by_user(poll, request.user.username):
        return HttpResponseForbidden()
    context = {'poll': poll}
    return HttpResponse(template.render(context, request),
                        content_type="text/plain; charset=utf-8")


@login_required
def poll_reminders(request, slug):
    simulate_send_emails.delay(10)
    return HttpResponse('Emails sended using Celery')


@login_required
def poll_edit(request, slug):
    template = loader.get_template('consensus_base/poll_edit.html')
    # TODO, make it a decorator
    poll = get_poll_by_uuid_or_slug(slug=slug)

    if not is_admin(poll, request.user.username):
        return HttpResponseForbidden()
    context = {'poll': poll}
    return HttpResponse(template.render(context, request))


@login_required
def poll_edit_invitations(request, slug):
    template = loader.get_template('consensus_base/poll_edit_invitations.html')
    # TODO, make it a decorator
    poll = get_poll_by_uuid_or_slug(slug=slug)

    if not is_admin(poll, request.user.username):
        return HttpResponseForbidden()

    if request.method == 'POST':
        emails = request.POST.get("emails")
        if emails:
            data = emails.split(",")
            validated_emails = set(filter(lambda e: validate_email(e), data))
            create_invitations(poll, validated_emails,
                               username=request.user.username)
        else:
            form = UploadFileForm(request.POST, request.FILES)
            if form.is_valid():
                file = request.FILES['file']
                data = []
                line = file.readline()
                while line:
                    aux = line.decode("utf8").replace(";", " ")
                    aux = aux.replace(",", " ")
                    data += aux.split()
                    line = file.readline()
                validated = set(filter(lambda e: validate_email(e), data))
                create_invitations(poll, validated,
                                   username=request.user.username)
        return redirect('consensus_base:poll_settings', slug=slug)
    else:
        form = UploadFileForm()
        context = {'poll': poll, 'form': form}
        return HttpResponse(template.render(context, request))


@login_required
def poll_settings(request, slug):
    template = loader.get_template('consensus_base/poll_settings.html')
    # TODO, make it a decorator
    poll = get_poll_by_uuid_or_slug(slug=slug)

    if not is_admin(poll, request.user.username):
        return HttpResponseForbidden()
    if request.method == 'POST':
        form = PollSettingsForm(request.POST, instance=poll)
        if form.is_valid():
            poll = form.save(commit=True)
            create_poll_change(poll, "poll_settings_updated")
            return redirect('consensus_base:poll_settings', slug=poll.slug)
    else:
        form = PollSettingsForm(instance=poll)
    context = {}
    context['poll'] = poll
    context['participants'] = poll.pollparticipant_set.order_by('username')
    context['invitations'] = poll.pollinvitation_set.order_by('username')
    context['form'] = form
    return HttpResponse(template.render(context, request))


@login_required
def poll_create(request):
    template = loader.get_template('consensus_base/poll_create.html')
    if request.method == 'POST':
        form = PollForm(request.POST)
        if form.is_valid():
            poll = form.save(commit=False)
            poll.creatorid = request.user.id
            poll.creator = request.user.username
            poll.save()
            return redirect('consensus_base:poll_edit', slug=poll.slug)
    else:
        form = PollForm()
    context = {'form': form}
    return HttpResponse(template.render(context, request))


@sort_by
@login_required
def polls(request):
    pag_items = getattr(s, "CONSENSUS_POLLS_PAGINATOR_ITEMS", 25)
    page = request.GET.get('page')

    polls = Poll.objects.where_is_visible(request.user.username)

    if request.sort_by:
        polls = polls.order_by(request.sort_by)
    else:
        polls = polls.order_by('-enddate', '-id')

    paginator = Paginator(
        polls,
        pag_items
    )

    polls = paginator_page(paginator, page)

    c = {
        'paginated_items': polls
    }
    return render(request, 'consensus_base/polls.html', c)

@sort_by
@login_required
def json_invitations(request):
    pag_items = getattr(s, "CONSENSUS_POLLS_PAGINATOR_ITEMS", 25)
    try:
        page = int(request.GET.get('page'))
    except Exception:
        page = 1
    objs = PollInvitation.objects
    invitations = objs.actives().filter(email=request.user.email)
    if request.sort_by:
        invitations = invitations.order_by(request.sort_by)
    else:
        invitations = invitations.order_by('-sentdate')

    pages = len(invitations)
    page_start = (page - 1) * pag_items
    page_end = min(page_start + pag_items, pages)
    data = InvitationSerializer().serialize(invitations[page_start:page_end])
    return JsonResponse({'items': data, 'page': page, 'pages': pages})


@sort_by
@login_required
def json_poll_settings(request):
    slug = request.GET.get('slug')
    poll = get_poll_by_uuid_or_slug(slug=slug)
    if not is_admin(poll, request.user.username):
        return HttpResponseForbidden()
    polls = PollSerializer(None).serialize([poll])
    participants = poll.pollparticipant_set.order_by('username')
    invitations = poll.pollinvitation_set.order_by('username')
    poll_participants = ParticipantsSerializer(poll).serialize(participants)
    poll_invitations = InvitationSerializer().serialize(invitations)
    return JsonResponse({'poll': polls[0], 'participants': poll_participants, 'invitations': poll_invitations})


@login_required
def json_poll_status(request, uuid=None):
    data = {}
    poll = Poll.objects.get(uuid=uuid)
    if not is_poll_visible_by_user(poll, request.user.username):
        return HttpResponseForbidden()
    data['overall'] = get_overall_status(poll.id)
    data['questions'] = get_questions_status(poll.id, request.user.id,
                                             show_comments=True)
    return JsonResponse(data)


def get_poll_by_uuid_or_slug(uuid=None, slug=None):
    if uuid:
        return Poll.objects.get(uuid=uuid)
    return Poll.objects.get(slug=slug)


@login_required
def json_poll_changes(request, uuid=None):
    poll = Poll.objects.get(uuid=uuid)
    if not is_poll_visible_by_user(poll, request.user.username):
        return HttpResponseForbidden()

    change_uuid = request.GET.get('last_change_uuid', None)

    if not is_valid_uuid(uuid):
        uuid = None
    poll_uuid = uuid

    if not is_valid_uuid(change_uuid):
        change_uuid = None
    data = {}
    change_found = False
    change = PollChange.objects
    if change_uuid and poll_uuid:
        change = change.filter(uuid=change_uuid, poll__uuid=poll_uuid)
        change_found = len(change)
    if not change_found and change_uuid:
        change = change.filter(uuid=change_uuid)
        change_found = len(change)
    if not change_found and poll_uuid:
        change = change.filter(poll__uuid=poll_uuid)
        change_found = len(change)
    if not change_found:
        data = {}
        data['return'] = -1
        data['value'] = {}
        return JsonResponse(data)

    change = change.order_by('-ctime')[0]
    changes = PollChange.objects.filter(
        poll=change.poll,
        ctime__gt=change.ctime).order_by('ctime')
    data = {}
    data['return'] = 0
    data['value'] = {}
    data['value']['previous_last_change_uuid'] = change.uuid.hex
    data['value']['poll_uuid'] = change.poll.uuid.hex
    data['value']['changes'] = []
    for c in changes:
        c1 = {}
        c1["ctime"] = from_date_to_timestamp(c.ctime)
        c1["change"] = c.change
        if 'user_answer' in c1["change"]['value']:
            c1["change"]['value']["user_answer"] = \
                get_user_answer(c.change['value']['uuid'], request.user.id)
            if not c1["change"]['value']["user_answer"]:
                c1["change"]['value'].pop("user_answer")
        c1["uuid"] = c.uuid.hex
        c1["poll_uuid"] = c.poll.uuid.hex
        data['value']['changes'].append(c1)
    if len(data['value']['changes']) > 0:
        data['value']['last_change_uuid'] = \
            data['value']['changes'][-1]['uuid']
    return JsonResponse(data)


@csrf_exempt
@login_required
@sort_by
def json_polls(request):
    try:
        page = int(request.GET.get('page', 1))
    except Exception:
        page = 1

    polls = Poll.objects.where_is_visible(request.user.username)

    if request.sort_by:
        polls = polls.order_by(request.sort_by)
    else:
        polls = polls.order_by('-enddate', '-id')

    pages = len(polls)
    page_items = getattr(s, "CONSENSUS_POLLS_PAGINATOR_ITEMS", 25)
    page_start = (page - 1) * page_items
    page_end = min(page_start + page_items, pages)
    data = PollSerializer(request.user.username).serialize(polls[page_start:page_end])
    return JsonResponse({'items': data, 'page': page, 'pages': pages})


@csrf_exempt
@login_required
def json_comment(request):
    '''
    Examples:
      { 'action': 'create', 'question': 'q1', 'value': '' }
      { 'action': 'create', 'question': 'q1'}
      { 'action': 'update', 'comment': '1', 'value': '' }
      { 'action': 'delete', 'comment': '1'}
    '''

    if not request.is_ajax():
        return HttpResponseForbidden()

    res = {}
    data = {}

    if 'action' in request.POST:
        data['action'] = request.POST['action']
    if 'question' in request.POST:
        data['question'] = request.POST['question']
    if 'comment' in request.POST:
        data['comment'] = request.POST['comment']
    if 'value' in request.POST:
        data['value'] = request.POST['value']

    if 'action' not in data:
        res['return'] = -1
        res['message'] = "No action"
        return JsonResponse(res)

    if data['action'] == 'delete':
        if 'comment' not in data:
            res['return'] = -1
            res['message'] = "No comment reference"
            return JsonResponse(res)

        comment_list = PollQuestionComment.objects.filter(uuid=data['comment'])

        if len(comment_list) > 0:
            comment = comment_list[0]
            if comment.question.section.poll.locked:
                res['return'] = -1
                res['message'] = "Poll is locked"
                return JsonResponse(res)
            if (comment.userid == request.user.id
                or is_admin(comment.question.section.poll,
                            request.user.username)):
                res['value'] = get_comment_status(comment)
                comment.delete()
                create_poll_change(comment.question.section.poll,
                                   "comment_deleted", res['value'])
                res['return'] = 0
                return JsonResponse(res)
            else:
                res['return'] = -1
                res['message'] = "Not authorized"
                return JsonResponse(res)
        else:
            res['return'] = -1
            res['message'] = "No choice doesnt exist"
            return JsonResponse(res)

    if data['action'] == 'update':
        if 'comment' not in data:
            res['return'] = -1
            res['message'] = "No comment reference"
            return JsonResponse(res)
        if 'value' not in data:
            res['return'] = -1
            res['message'] = "No comment value submitted"
            return JsonResponse(res)

        comment_list = PollQuestionComment.objects.filter(uuid=data['comment'])

        if len(comment_list) > 0:
            comment = comment_list[0]
            if comment.question.section.poll.locked:
                res['return'] = -1
                res['message'] = "Poll is locked"
                return JsonResponse(res)
            if (comment.userid == request.user.id
                or is_admin(comment.question.section.poll,
                            request.user.username)):
                comment.comment = data['value']
                comment.save()
                res['value'] = get_comment_status(comment)
                res['return'] = 0
                create_poll_change(comment.question.section.poll,
                                   "comment_updated", res['value'])
                return JsonResponse(res)
            else:
                res['return'] = -1
                res['message'] = "Not authorized"
                return JsonResponse(res)
        else:
            res['return'] = -1
            res['message'] = "Comment doesnt exist"
            return JsonResponse(res)

    if data['action'] == 'create':
        if 'question' not in data:
            res['return'] = -1
            res['message'] = "No question reference"
            return JsonResponse(res)

        question_list = PollQuestion.objects.filter(uuid=data['question'])
        if len(question_list) > 0:
            question = question_list[0]
            if question.section.poll.locked:
                res['return'] = -1
                res['message'] = "Poll is locked"
                return JsonResponse(res)
            if (is_participant(question.section.poll, request.user.username)
                    or is_admin(question.section.poll, request.user.username)):
                comment = PollQuestionComment()
                comment.comment = data['value']
                comment.question = question
                comment.userid = request.user.id
                comment.username = request.user.username
                comment.save()
                res['value'] = get_comment_status(comment)
                res['return'] = 0
                create_poll_change(comment.question.section.poll,
                                   "comment_created", res['value'])
                return JsonResponse(res)
            else:
                res['return'] = -1
                res['message'] = "Not authorized"
                return JsonResponse(res)
        else:
            res['return'] = -1
            res['message'] = "Question doesnt exist"
            return JsonResponse(res)


@csrf_exempt
@login_required
def json_answer(request):
    '''
    Examples:
      { 'action': 'update', 'question': 'q1', choice: '1', 'value': True }
    '''

    if not request.is_ajax():
        return HttpResponseForbidden()

    res = {}
    data = {}

    if 'action' in request.POST:
        data['action'] = request.POST['action']
    if 'question' in request.POST:
        data['question'] = request.POST['question']
    if 'choice' in request.POST:
        data['choice'] = request.POST['choice']
    if 'value' in request.POST:
        data['value'] = request.POST['value']

    if 'action' not in data:
        res['return'] = -1
        res['message'] = "No action"
        return JsonResponse(res)

    if 'question' not in data:
        res['return'] = -1
        res['message'] = "No question reference"
        return JsonResponse(res)

    if 'choice' not in data:
        res['return'] = -1
        res['message'] = "No choice reference"
        return JsonResponse(res)

    if 'value' not in data:
        res['return'] = -1
        res['message'] = "No value submitted"
        return JsonResponse(res)

    q = PollQuestion.objects.get(uuid=data['question'])
    if not q.choices:
        q.choices = {}

    if not is_participant(q.section.poll, request.user.username):
        res['return'] = -1
        res['message'] = "No participant role"
        return JsonResponse(res)

    if q.section.poll.locked:
        res['return'] = -1
        res['message'] = "Poll is locked"
        return JsonResponse(res)

    a, _ = PollQuestionAnswer.objects.get_or_create(question=q,
                                                    userid=request.user.id)

    if data['action'] == 'update':
        if data['choice'] not in q.choices:
            res['return'] = -1
            res['message'] = "No valid choice submitted"
            return JsonResponse(res)

        a.answer = {}
        a.answer[data['choice']] = data['value']
        a.username = request.user.username
        a.save()
        question_status = get_question_status(q, userid=request.user.id,
                                              show_comments=False)
        res['value'] = question_status
        res['return'] = 0
        create_poll_change(q.section.poll, "question_answered", res['value'])
        return JsonResponse(res)
    else:
        res['return'] = -1
        res['message'] = "Action not implemented"
        return JsonResponse(res)


@csrf_exempt
@login_required
def json_choice(request):
    '''
    Examples:
      { 'action': 'create', 'question': 'q1', choice: '1', 'value': '' }
      { 'action': 'create', 'question': 'q1', choice: '1'}
      { 'action': 'create', 'question': 'q1'}
      { 'action': 'update', 'question': 'q1', choice: '1', 'value': '' }
      { 'action': 'delete', 'question': 'q1', choice: '1'}
    '''

    if not request.is_ajax():
        return HttpResponseForbidden()

    res = {}
    data = {}

    if 'action' in request.POST:
        data['action'] = request.POST['action']
    if 'question' in request.POST:
        data['question'] = request.POST['question']
    if 'choice' in request.POST:
        data['choice'] = request.POST['choice']
    if 'value' in request.POST:
        data['value'] = request.POST['value']

    if 'action' not in data:
        res['return'] = -1
        res['message'] = "No action"
        return JsonResponse(res)

    if 'question' not in data:
        res['return'] = -1
        res['message'] = "No question reference"
        return JsonResponse(res)

    q = PollQuestion.objects.get(uuid=data['question'])

    if not is_admin(q.section.poll, request.user.username):
        res['return'] = -1
        res['message'] = "No admin role"
        return JsonResponse(res)

    if not q.choices:
        q.choices = {}

    if data['action'] == 'delete':
        if 'choice' not in data:
            res['return'] = -1
            res['message'] = "No choice reference"
            return JsonResponse(res)
        if data['choice'] in q.choices:
            q.choices.pop(data['choice'])
            q.save()
            question_status = get_question_status(q, userid=request.user.id,
                                                  show_comments=False)
            res['value'] = question_status
            res['return'] = 0
            create_poll_change(q.section.poll, "choice_deleted", res['value'])
            return JsonResponse(res)
        else:
            res['return'] = -1
            res['message'] = "No choice doesnt exist"
            return JsonResponse(res)

    if data['action'] == 'update':
        if 'choice' not in data:
            res['return'] = -1
            res['message'] = "No choice reference"
            return JsonResponse(res)
        if 'value' not in data:
            res['return'] = -1
            res['message'] = "No choice value submitted"
            return JsonResponse(res)
        q.choices[data['choice']] = data['value']
        q.save()
        question_status = get_question_status(q, userid=request.user.id,
                                              show_comments=False)
        res['value'] = question_status
        res['return'] = 0
        create_poll_change(q.section.poll, "choice_updated", res['value'])
        return JsonResponse(res)

    if data['action'] == 'create':
        if 'choice' in data:
            if data['choice'] in q.choices:
                res['return'] = -1
                res['message'] = "Choice reference already exists"
                return JsonResponse(res)
        else:
            if len(q.choices) == 0:
                new_choice = 1
            else:
                new_choice = max(map(lambda x: int(x), q.choices.keys())) + 1
            data['choice'] = new_choice
        if 'value' not in data:
            data['value'] = ''
        q.choices[data['choice']] = data['value']
        q.save()
        question_status = get_question_status(q, userid=request.user.id,
                                              show_comments=False)
        res['value'] = question_status
        res['return'] = 0
        create_poll_change(q.section.poll, "choice_created", res['value'])
        return JsonResponse(res)


@csrf_exempt
@login_required
def json_question(request):
    '''
    Examples:
      { 'action': 'create,  'poll': 'p1', 'values': { ... }}
      { 'action': 'update', 'question': 'q1', 'values': { ... }}
      { 'action': 'delete', 'question': 'q1 }
    '''

    def _update_values(question, values):
        if 'question' in values:
            question.question = values['question']
        if 'help' in values:
            question.help = values['help']
        if 'hide' in values:
            question.hide = values['hide']
        if 'mandatory' in values:
            question.mandatory = values['mandatory']

    if not request.is_ajax():
        return HttpResponseForbidden()

    res = {}
    data = {}

    if 'action' in request.POST:
        data['action'] = request.POST['action']
    if 'question' in request.POST:
        data['question'] = request.POST['question']
    if 'poll' in request.POST:
        data['poll'] = request.POST['poll']
    if 'values' in request.POST:
        data['values'] = json.loads(request.POST['values'])
    if 'action' not in data:
        res['return'] = -1
        res['message'] = "No action"
        return JsonResponse(res)

    if data['action'] == 'delete':
        q = PollQuestion.objects.get(uuid=data['question'])
        if not is_admin(q.section.poll, request.user.username):
            res['return'] = -1
            res['message'] = "No admin role"
            return JsonResponse(res)
        q.delete()
        question_status = get_question_status(q, userid=request.user.id,
                                              show_comments=False)
        res['value'] = question_status
        res['return'] = 0
        create_poll_change(q.section.poll, "question_deleted", res['value'])
        return JsonResponse(res)
    if data['action'] == 'update':
        q = PollQuestion.objects.get(uuid=data['question'])
        if not is_admin(q.section.poll, request.user.username):
            res['return'] = -1
            res['message'] = "No admin role"
            return JsonResponse(res)
        if 'values' in data:
            _update_values(q, data['values'])
            q.save()
        question_status = get_question_status(q, userid=request.user.id,
                                              show_comments=True)
        res['value'] = question_status
        res['return'] = 0
        create_poll_change(q.section.poll, "question_updated", res['value'])
        return JsonResponse(res)
    if data['action'] == 'create':
        if 'poll' not in data:
            res['return'] = -1
            res['message'] = "No poll reference"
            return JsonResponse(res)
        q = PollQuestion()
        poll = Poll.objects.get(uuid=data['poll'])
        try:
            section = PollSection.objects.get(poll=poll)
        except Exception:
            section = PollSection(poll=poll, title="Section")
            section.save()
        q.section = section
        if not is_admin(poll, request.user.username):
            res['return'] = -1
            res['message'] = "No admin role"
            return JsonResponse(res)
        if 'values' in data:
            _update_values(q, data['values'])
        q.save()
        question_status = get_question_status(q, userid=request.user.id,
                                              show_comments=False)
        res['value'] = question_status
        res['return'] = 0
        create_poll_change(q.section.poll, "question_created", res['value'])
        return JsonResponse(res)
