# -*- coding: utf-8 -*-
from __future__ import absolute_import

from django import forms
# from django.conf import settings as s
from django.contrib.auth import authenticate
from django.utils.translation import ugettext as _

from .utils import get_groups
from .utils import get_users
from .helpers import set_participants
from .models import Poll
from .models import PollParticipant
from .models import PollGroupParticipant
from django.utils.translation import gettext as _


class BooleanSelect(forms.Select):
    """
    A Select Widget intended to be used with BooleanField.
    """
    def __init__(self, attrs=None):
        choices = (
            ('1', _('No')),
            ('2', _('Yes')),
        )
        super(BooleanSelect, self).__init__(attrs, choices)

    def format_value(self, value):
        try:
            return {True: '2', False: '1', '2': '2'}[value]
        except KeyError:
            return '1'

    def value_from_datadict(self, data, files, name):
        value = data.get(name)
        return {
            '1': False,
            '2': True,
            True: True,
            'True': True,
            False: False,
            'False': False,
        }.get(value)


class PollForm(forms.ModelForm):
    class Meta:
        model = Poll
        fields = ['title', 'description']
        # fields += ['public']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            # 'public': BooleanSelect(attrs={'class': 'form-control'}),
        }


class PollSettingsForm(forms.ModelForm):
    groups = forms.MultipleChoiceField(
        choices=map(lambda x: (x,x), get_groups(sorted=True)),
        widget=forms.SelectMultiple(attrs={'class': 'form-control'}),
        label = "Group of participants",
        required = False,
    )
    users = forms.MultipleChoiceField(
        choices=map(lambda x: (x,x), get_users(sorted=True)),
        widget=forms.SelectMultiple(attrs={'class': 'form-control', 'style': 'height: 300px'}),
        label = "Participants",
        required = False,
    )

    class Meta:
        model = Poll
        fields = ['title', 'description', 'published', 'locked']
        # fields += ['public', 'startdate', 'enddate']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            # 'public': BooleanSelect(attrs={'class': 'form-control'}),
            'published': BooleanSelect(attrs={'class': 'form-control'}),
            'locked': BooleanSelect(attrs={'class': 'form-control'}),
            # 'startdate': forms.DateTimeInput(attrs={'type': 'datetime-local', 'class': 'form-control'}),
            # 'enddate': forms.DateTimeInput(attrs={'type': 'datetime-local', 'class': 'form-control'}),

        }

    def __init__(self, *args, **kwargs):
        super(PollSettingsForm, self).__init__(*args, **kwargs)
        if kwargs['instance']:
            u = list(map(lambda x: x.username, kwargs['instance'].pollparticipant_set.all()))
            self.fields['users'].initial = u
            g = list(map(lambda x: x.group, kwargs['instance'].pollgroupparticipant_set.all()))
            self.fields['groups'].initial = g

    def save(self, commit=True):
        instance = super(PollSettingsForm, self).save(commit=commit)
        users = self.cleaned_data.get('users', [])
        groups = self.cleaned_data.get('groups', [])
        if commit:
            set_participants(instance, groups, users)
        return instance


class PollSettingsSimpleForm(forms.ModelForm):
    class Meta:
        model = Poll
        fields = ['title', 'description', 'published', 'locked','customreminder']
        # fields += ['public', 'startdate', 'enddate']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            # 'public': BooleanSelect(attrs={'class': 'form-control'}),
            'published': BooleanSelect(attrs={'class': 'form-control'}),
            'locked': BooleanSelect(attrs={'class': 'form-control'}),
            'customreminder':forms.Textarea(attrs={'class': 'form-control'})
            # 'startdate': forms.DateTimeInput(attrs={'type': 'datetime-local', 'class': 'form-control'}),
            # 'enddate': forms.DateTimeInput(attrs={'type': 'datetime-local', 'class': 'form-control'}),

        }
    
    def __init__(self, *args, **kwargs):
        super(PollSettingsSimpleForm, self).__init__(*args, **kwargs)
        if not self.initial['customreminder']:
            self.initial['customreminder'] = _('We have noticed that you still have pending polls to answer...')

    def save(self, commit=True):
        instance = super(PollSettingsSimpleForm, self).save(commit=commit)
        return instance

    def clean_customreminder(self):
        reminder = self.cleaned_data['customreminder']
        if reminder == _('We have noticed that you still have pending polls to answer...'):
            reminder = ''
        return reminder



class ChangePasswordForm(forms.Form):

    auth_password = forms.CharField(widget=forms.PasswordInput(),
                                    label=_('Your master password'))
    new_password_1 = forms.CharField(widget=forms.PasswordInput(),
                                     label=_('New password'))
    new_password_2 = forms.CharField(widget=forms.PasswordInput(),
                                     label=_('Confirm password'))

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean(self):
        auth_password = self.cleaned_data.get('auth_password')
        new_password_1 = self.cleaned_data.get('new_password_1')
        new_password_2 = self.cleaned_data.get('new_password_2')

        user = authenticate(username=self.user.username,
                            password=auth_password)
        if not user:
            raise forms.ValidationError(_("Incorrect master password"))

        if len(new_password_1) > 0 and len(new_password_2) == 0:
            raise forms.ValidationError(_("You must confirm password"))

        if new_password_1 != new_password_2:
            raise forms.ValidationError(_("Passwords did not match"))

        if auth_password == new_password_1:
            raise forms.ValidationError(_("New password and the master password can't be the same"))  # noqa:E501

        try:
            cracklib.VeryFascistCheck(new_password_1)
        except ValueError as e:
            raise forms.ValidationError(
                "Password %s." % str(e)[3:])

        return self.cleaned_data


class UploadFileForm(forms.Form):
    file = forms.FileField()
