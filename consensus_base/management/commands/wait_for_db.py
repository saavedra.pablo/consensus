import time

from django.db import connections
from django.db.utils import OperationalError
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Django command to pause execution until database is available"""

    def handle(self, *args, **kwargs):
        self.stdout.write('Waiting for database...')
        db_connected = False
        while not db_connected:
            try:
                db_conn = connections['default']
                db_conn.connect()
                db_connected = True
            except Exception:
                self.stdout.write('Database unavailable, waiting 1 second...')
                time.sleep(1)
        self.stdout.write(self.style.SUCCESS('Database available'))
