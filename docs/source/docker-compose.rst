Setup Consensus with Docker compose
===================================

.. toctree::
   :maxdepth: 2
   :caption: Other related content:

Installing the Docker runtime
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can follow the `Docker's official site instructions <https://docs.docker.com/engine/install/ubuntu>`_
(recommended) or follow the next steps.

You need to run docker-ce (17 or higher). Older versions of Docker were called
docker, docker.io, or docker-engine. If these are installed, uninstall them::

    sudo apt-get remove docker docker-engine docker.io containerd runc
    sudo apt-get update

Then install Docker from Docker's APT repositories::

    sudo apt-get update
    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io

Installing Docker Compose
^^^^^^^^^^^^^^^^^^^^^^^^^

You have two choices:

* The most straightforward method is to use the package provided by your
  distro::

      sudo apt-get install docker-compose

* And the `official <https://docs.docker.com/compose/install/>`_ to get the
  latest version with the latest features::

      sudo curl -L "https://github.com/docker/compose/releases/download/1.27.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
      sudo chmod +x /usr/local/bin/docker-compose

Both methods should work with Consensus if you are using any distro not noo
much outdated. Whatever method you use, test the installation like follows::

    docker-compose --version

Rolling out Consensus with Docker Compose
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The repository provides a `docker-compose.yml` to deploy Docker instances
for this project. You can use it copying the following files to the secrets
to the proper destination::

    cp django_consensus/local_settings.py.sample docker-compose/secrets/local_settings.py
    cp setup-env.sh.sample.postgres setup-env.sh
    cp docker-compose/app/conf/redis.conf docker-compose/secrets/
    cp docker-compose/app/conf/supervisor-redis.conf.sample docker-compose/secrets/supervisor-redis.conf
    cp docker-compose/app/conf/supervisor-celery.conf.sample docker-compose/secrets/supervisor-celery.conf

And then using the following commands to run, check, scale, login and so the
Docker instances running your Consensus instance::

    tracie_dashboard$ ./docker-compose.sh up [--scale app=2] [-d]
    tracie_dashboard$ ./docker-compose.sh logs
    tracie_dashboard$ ./docker-compose.sh scale app=4
    tracie_dashboard$ ./docker-compose.sh ps
    tracie_dashboard$ ./docker-compose.sh exec --index=2 app /bin/bash

Create a superuser::

    tracie_dashboard$ ./docker-compose.sh exec --index=2 app ./manage.py createsuperuser

The service will be available in http://127.0.0.1

Finally, you can stop the docker running this command::

    tracie_dashboard$ ./docker-compose.sh stop

