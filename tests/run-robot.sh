#! /bin/bash -x
V="v0.28.0"
TARBALL="geckodriver-${V}-linux64.tar.gz"
wget https://github.com/mozilla/geckodriver/releases/download/${V}/${TARBALL}
tar -xvf ${TARBALL}
rm ${TARBALL}
export PATH=$(pwd):$PATH

virtualenv venv_robot_framework
. ./venv_robot_framework/bin/activate

pip install robotframework \
		    robotframework-seleniumlibrary

mkdir -p tests_results/robot_framework/
cd tests_results/robot_framework/

robot ../../tests/robot_framework/tests.robot

