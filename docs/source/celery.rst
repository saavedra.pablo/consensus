How To Configure and extend Celery task queue
=============================================

Installation and setup
^^^^^^^^^^^^^^^^^^^^^^

First, the default installation uses Redis as backend for Celery. You can check
this in the `settings.py`::

    # CELERY STUFF
    CELERY_BROKER_URL='redis://127.0.0.1:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379'
    CELERY_ACCEPT_CONTENT = ['application/json']
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_RESULT_SERIALIZER = 'json'
    CELERY_TIMEZONE = 'UTC'

Therefore we need installed and running a Redis instance in the host::

    apt-get install redis-server redis-tools

Second, Celery is listed as a project dependency in the `requirements.txt`
file and it should be installed by the pip command. We will need 2 daemons
running as services in the host:

* The Celery Beat what is the periodic task scheduler. This is required to
  schedule the configured and or triggered tasks::

    /opt/django_consensus/env/bin/celery -A django_consensus beat

* A Celery Worker to process the tasks::

    /opt/django_consensus/env/bin/celery -A django_consensus worker

In the log output of both daemons you will see how the tasks will be
allocated and how they will be executed by the Celery worker.


Setup Celery task queue
^^^^^^^^^^^^^^^^^^^^^^^

The Celery application is loaded in the init of the Consensus application
`django_consensus/__init__.py`::

    from __future__ import absolute_import, unicode_literals
    from .celery import app as celery_app
    __all__ = ('consensus',)


It is in the `django_consensus/celery.py` where the Celery application is
initiated and configured according with the `CELERY_` settings::

  ...
  os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_consensus.settings')
  app = Celery('django_consensus')
  app.config_from_object('django.conf:settings', namespace='CELERY')


The Celery application is instructed to autodiscover all the Celery tasks
defined in the loaded applications::

  app.autodiscover_tasks()


For sake of clarity, the Celery tasks are defined in the  `tasks.py`
modules. For example, in `consensus_base/tasks.py`:


Create a new task
^^^^^^^^^^^^^^^^^

In a `tasks.py` (example: `consensus_base/tasks.py`) file::

  from consensus_base.mail import send_invitations

  @shared_task
  def foo():
      return 'Do something'


A shared tasks can be trigered by the Consensus logic, for example as part
of the logic of a view::

    from .tasks import foo
    ...

    def my_view(request):
        foo.delay(10)
        return HttpResponse('Called a shared tasks')

In the other hand there are the *periodic tasks*::

  @periodic_task(run_every=(crontab(minute='*/1')), ignore_result=False)
  def cron_action():
      logger.info("Do something every 1 minute")
      return 'Something'

; these tasks differs from the *shared tasks* in the way of they are not
associated to any incoming request to the server. Instead of that, they are
triggered by an internal scheduler instantiated in the Celery beat process.

